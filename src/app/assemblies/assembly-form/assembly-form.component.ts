import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { IVMAssembly } from '../../model/viewModel/assemblyViewModels/vmAssembly';
import { IVMAssemblyDesignation } from '../../model/viewModel/assemblyViewModels/vmAssemblyDesignation';
import { IVMSubAssembly } from '../../model/viewModel/assemblyViewModels/vmSubAssembly';
import { IDesignation } from '../../model/designation';
import { style } from '@angular/animations';
import { DesignationService } from '../../core/services/designation.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { AssemblyService } from '../../core/services/assembly.service';
import { MessageType, MessageBoxComponent } from '../../shared/message-box/message-box.component';
import { DialogService } from 'ng2-bootstrap-modal';
import { AppError } from '../../errorhandlers/app-error';
import { NotFoundError } from '../../errorhandlers/not-found-error';
import { BadRequestError } from '../../errorhandlers/bad-request-error';
import { Global } from '../../core/services/global';
import { MessageService } from 'primeng/components/common/messageservice';
import {Message} from 'primeng/components/common/api';
import { toastMessage } from '../../model/toastMessage';
import { AssemblyCategoryService } from '../../core/services/assemblyCategory.service';
import { IAssemblyCategory } from '../../model/assemblyCategory';


interface ISelectionListItem {
  id: number,
  itemName: string
}

@Component({
  selector: 'app-assembly-form',
  templateUrl: './assembly-form.component.html',
  styleUrls: ['./assembly-form.component.css'],
  //providers: [MessageService]
})
export class AssemblyFormComponent implements OnInit {


  form: FormGroup;
  private subAssemblySequenceNo:number = 0;
  private sub: Subscription;
  //loading: boolean = false;
  assembly: IVMAssembly = {
    "id": 0,
    "assemblyName": "",
    "assemblyDescription": "",
    "duration": 0, 
    "assemblyDesignations": [],
    "subAssemblies": [],
    "categoryId":0,
    "categoryName":''
  }

  designationsList: ISelectionListItem[] = [];
  selectedDesignations: ISelectionListItem[] = [];
  dropdownSettings = {};
  errorMessage: string;
  desigListFromServer: IDesignation[];
  assemblyCategories: IAssemblyCategory[];
  selectedAssemblyCategory: IAssemblyCategory;

  constructor(private fb: FormBuilder,
    private designationService: DesignationService,
    private assemblyService: AssemblyService,
    private router: Router,
    private activateRoute: ActivatedRoute,
    private dialogService: DialogService,
    private messageService: MessageService,
    private assemblyCategoryService: AssemblyCategoryService
    ) {
     
    this.form = fb.group({
      assemblyName: ['', Validators.required],
      assemblyDescription: [],
      durationInMins: ['', [Validators.required, Validators.pattern('^[0-9]+$')]],
      assemblyCategory:['',Validators.required],
      assemblyDesignations: [[]],
      subAssemblies: [[]]
    });

  }

  ngOnInit() {
    let id = 0;
    this.dropdownSettings = {
      text: "Select Designations",
      enableCheckAll: false,
      classes: "noBorder",
      showCheckbox: true
    };
    this.sub = this.activateRoute.params.subscribe(
      params => {
        id = +params['id'];
        if (Number.isNaN(id) == false) {
          this.getAssembly(id);
        }
      });
      if (Number.isNaN(id)){
      this.getAllDesignations();
      this.getAllAssemblyCategories();
      }
    
  }

  addSubAssembly(asm) {
    //alert(assembly.assemblyName);
    //console.log(assembly.id);
    let subAssembly: IVMSubAssembly = {
      "assemblyId": this.assembly.id,
      "subAssemblyId": asm.id,
      "subAssemblyName": asm.assemblyName,
      "qty": 1,
      "duration": asm.duration,
      "sequenceNo":this.subAssemblySequenceNo + 1
    }
    
    this.durationInMins.setValue(parseInt(this.durationInMins.value) + parseInt(asm.duration));

    let subAssemblyFound = this.assembly.subAssemblies.find(a => a.subAssemblyId == asm.id)

    if (subAssemblyFound === undefined) {
      this.assembly.subAssemblies.push(subAssembly);
      this.subAssemblySequenceNo = this.subAssemblySequenceNo + 1;
    } else {
      subAssemblyFound.qty = subAssemblyFound.qty + 1;
      subAssemblyFound.duration = subAssemblyFound.duration + asm.duration;
    }
  }

  removeSubAssembly(asm) {
    //console.log(asm);
    let index = this.assembly.subAssemblies.findIndex(a => a.subAssemblyId == asm.subAssemblyId);
    if (index >= 0) {
      this.durationInMins.setValue(parseInt(this.durationInMins.value) - this.assembly.subAssemblies[index].duration);
      this.assembly.subAssemblies.splice(index, 1);
    }
    //Set the sequence numbers of all remaining sub assemblies
    if (this.assembly.subAssemblies.length === 0) return
    let seqNo = 0;
    this.assembly.subAssemblies.forEach(subAsm => {
      seqNo = seqNo + 1;
      subAsm.sequenceNo = seqNo;
    });
    this.subAssemblySequenceNo = seqNo;
  }

  moveUp(i){
    if (i === 0) return;
    this.assembly.subAssemblies[i].sequenceNo = this.assembly.subAssemblies[i].sequenceNo -1;
    this.assembly.subAssemblies[i-1].sequenceNo = this.assembly.subAssemblies[i-1].sequenceNo+1;
    let asm1 = Object.assign({},this.assembly.subAssemblies[i]);
    this.assembly.subAssemblies[i]= Object.assign({},this.assembly.subAssemblies[i-1]);
    this.assembly.subAssemblies[i-1] = Object.assign({},asm1);
  }

  moveDown(i){
    if (i === this.assembly.subAssemblies.length-1) return;
    this.assembly.subAssemblies[i].sequenceNo = this.assembly.subAssemblies[i].sequenceNo +1;
    this.assembly.subAssemblies[i+1].sequenceNo = this.assembly.subAssemblies[i+1].sequenceNo-1;
    
    let asm1 = Object.assign({},this.assembly.subAssemblies[i]);
    this.assembly.subAssemblies[i]= Object.assign({},this.assembly.subAssemblies[i+1]);
    this.assembly.subAssemblies[i+1] = Object.assign({},asm1);
  }

  getAssembly(id: number): void {
    Global.setLoadingFlag(true);;
    this.assemblyService.getAssembly(id)
      .subscribe(responseData => {
        if (responseData.Success) {
          this.assembly = (<IVMAssembly>responseData.data[0]);
          //console.log(this.assembly);
          this.getAllDesignations();
          this.getAllAssemblyCategories();
          Global.setLoadingFlag(false);
          if (this.assembly.subAssemblies.length > 0){
            this.subAssemblySequenceNo = this.assembly.subAssemblies[this.assembly.subAssemblies.length-1].sequenceNo;
            }
        }
        else {
          Global.setLoadingFlag(false);
          this.showMessage(MessageType.Error, "Error", responseData.Message);
        }
      }),
      (error: AppError) => {
        Global.setLoadingFlag(false);
        if (error instanceof NotFoundError) {
          this.showMessage(MessageType.Error, "Error", "Requested data not found.");
        }
        else if (error instanceof BadRequestError) {
          this.showMessage(MessageType.Error, "Error", "Unable to process the request.");
        }
        else throw error;
      }
  }

  get assemblyName() { return this.form.get("assemblyName"); }
  get assemblyDescription() { return this.form.get("assemblyDescription"); }
  get durationInMins() { return this.form.get("durationInMins"); }
  get assemblyDesignations() { return this.form.get('assemblyDesignations') }
  get assemblyCategory() { return this.form.get("assemblyCategory"); }

  saveForm() {
    this.assembly.assemblyDesignations = [];
    this.selectedDesignations.forEach(desig => {
      let selDesig: IVMAssemblyDesignation = {
        assemblyId: this.assembly.id,
        designationId: desig.id,
        title: desig.itemName
      }
      this.assembly.assemblyDesignations.push(selDesig);
    });
    this.assembly.categoryId = this.selectedAssemblyCategory.id;
    if (this.assembly.id > 0) {
      this.assemblyService.updateAssembly(this.assembly)
      .subscribe(revData => {
        if (revData.Success){
          Global.setLoadingFlag(false);
          this.showToastMessage('success','','Assembly saved.');
         
        } else {
          Global.setLoadingFlag(false); 
          this.showMessage(MessageType.Error, "Error", revData.Message);
        }
      }),
      (error: AppError) => {
        Global.setLoadingFlag(false);
        if (error instanceof NotFoundError) {
          this.showMessage(MessageType.Error, "Error", "Requested data not found.");
        }
        else if (error instanceof BadRequestError) {
          this.showMessage(MessageType.Error, "Error", "Unable to process the request.");
        }
        else throw error;
      } 
    } else { 
      Global.setLoadingFlag(true);;
      this.assemblyService.createAssembly(this.assembly)
      .subscribe(revData => {
        if (revData.Success){
          this.assembly.id = revData.data[0];
          Global.setLoadingFlag(false);
          
         this.showToastMessage('success','','Assembly saved.');
         this.router.navigate(['assembly/' + this.assembly.id ]);
        } else {
          Global.setLoadingFlag(false);
          this.showMessage(MessageType.Error, "Error", revData.Message);
        }
      }),
      (error: AppError) => {
        Global.setLoadingFlag(false);
        if (error instanceof NotFoundError) {
          this.showMessage(MessageType.Error, "Error", "Requested data not found.");
        }
        else if (error instanceof BadRequestError) {
          this.showMessage(MessageType.Error, "Error", "Unable to process the request.");
        }
        else throw error;
      } 
    }
    
  }

  cancelForm() {
    this.router.navigate(['/assemblies']);
  }

  newForm(){
    this.router.navigate(['assemblies/new']);
  }

  getAllDesignations() {
    this.designationService.getDesignations().subscribe(
      designationData => {
        if (designationData.Success) {
          this.desigListFromServer = designationData.data;

        } else {
          this.showMessage(MessageType.Error, "Error", designationData.Message);
        }
      },
      (error: AppError) => {
        //Global.setLoadingFlag(false);
        if (error instanceof NotFoundError) {
          this.showMessage(MessageType.Error, "Error", "Requested data not found.");
        }
        else if (error instanceof BadRequestError) {
          this.showMessage(MessageType.Error, "Error", "Unable to process the request.");
        }
        else throw error;
      },
      () => {
        //console.log(this.designationsList);
        this.designationsList = [];
        this.desigListFromServer.forEach(desig => {
          let designation: ISelectionListItem = {
            id: desig.id,
            itemName: desig.title
          }
          this.designationsList.push(designation);
        });
        if (this.assembly.id > 0) {
          this.selectedDesignations = [];
          this.assembly.assemblyDesignations.forEach(desig => {
            let designationItem: ISelectionListItem = {
              id: desig.designationId,
              itemName: desig.title
            }
            this.selectedDesignations.push(designationItem);
          })

        }
      }
    )
  }


   //get all assembly categories 
   getAllAssemblyCategories() {
    Global.setLoadingFlag(true);
    this.assemblyCategoryService.getAssemblyCategories().subscribe(
      categoriesData => {
        if (categoriesData.Success) {
          this.assemblyCategories = categoriesData.data;
          Global.setLoadingFlag(false);
        } else {
          Global.setLoadingFlag(false);
          this.showMessage(MessageType.Error, "Error", categoriesData.Message);
        }
      },
      (error: AppError) => {
        Global.setLoadingFlag(false);
        if (error instanceof NotFoundError) {
          this.showMessage(MessageType.Error, "Error", "Requested data not found.");
        }
        else if (error instanceof BadRequestError) {
          this.showMessage(MessageType.Error, "Error", "Unable to process the request.");
        }
        else throw error;
      },()=>{
        if(this.assembly.id > 0){
         // console.log(this.assembly);
          this.selectedAssemblyCategory = this.assemblyCategories.find(c => c.id === this.assembly.categoryId);
         // this.selectedAssemblyCategory.id = this.assembly.categoryId;
          //this.selectedAssemblyCategory.categoryName = this.assembly.categoryName;
        }
      });
  }

  showMessage(messageType: MessageType, title: string, message: string) {

    let disposable = this.dialogService.addDialog(MessageBoxComponent, {
      title: title,
      messageType: messageType,
      message: message

    }).subscribe((isConfirmed) => { });
  }

  // checkmsg(){
  //   this.showToastMessage({severity:'info', summary:'Info Message', detail:'PrimeNG rocks'});
  // }

  showToastMessage(severity:string, summary: string, detail:string){
    let message: Message = {
      severity: severity,
      summary: summary,
      detail:detail
    }
    this.messageService.add(message);
  }
}
