import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssemblyCategoryComponent } from './assembly-category.component';

describe('AssemblyCategoryComponent', () => {
  let component: AssemblyCategoryComponent;
  let fixture: ComponentFixture<AssemblyCategoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssemblyCategoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssemblyCategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
