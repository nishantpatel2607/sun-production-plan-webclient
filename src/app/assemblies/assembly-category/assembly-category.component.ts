import { AssemblyCategoryService } from '../../core/services/assemblyCategory.service';

import { Component, OnInit } from '@angular/core'; 
import { ActivatedRoute, Router } from '@angular/router';



import { PagerService } from '../../core/services/pager.service';
import { DialogService } from 'ng2-bootstrap-modal';
import { MessageType, MessageBoxComponent } from '../../shared/message-box/message-box.component';
import { AppError } from '../../errorhandlers/app-error';
import { NotFoundError } from '../../errorhandlers/not-found-error';
import { BadRequestError } from '../../errorhandlers/bad-request-error';
import { Global } from '../../core/services/global';
import { toastMessage } from '../../model/toastMessage';
import { MessageService } from 'primeng/components/common/messageservice';
import { Message } from 'primeng/components/common/api';
import { IAssemblyCategory } from '../../model/assemblyCategory';

@Component({
  selector: 'assembly-category',
  templateUrl: './assembly-category.component.html',
  styleUrls: ['./assembly-category.component.css'] 
})
export class AssemblyCategoryComponent implements OnInit {

  errorMessage: string;
  private pagesize: number = 5; 

  selectedCategory: IAssemblyCategory = {
    id: 0,
    categoryName: ""
  }
  newCategory: IAssemblyCategory; //used to store new category added
  categoryName: string = ""; //stores Category texbox value
  categoryListFilter: string = "";//stores Category filter texbox value
  categories: IAssemblyCategory[];
  categoryPager: any = {};
  categoryPagedItems: IAssemblyCategory[];
  categoryFilteredItems: IAssemblyCategory[];

  constructor(private activeRoute: ActivatedRoute,
    private route: Router,
    private pagerService: PagerService,
    private assemblyCategoryService: AssemblyCategoryService,
    private dialogService: DialogService,
    private messageService: MessageService) { }

  ngOnInit() {
    this.getAssemblyCategories();
  }

  getAssemblyCategories() {
    Global.setLoadingFlag(true); 
    this.assemblyCategoryService.getAssemblyCategories()
      .subscribe(categoriesData => {
        if (categoriesData.Success) {
          this.categories = categoriesData.data;
          this.categoryFilteredItems = categoriesData.data;
          this.setPage(1);
          Global.setLoadingFlag(false);
        } else {
          Global.setLoadingFlag(false);
          this.showMessage(MessageType.Error, "Error", categoriesData.Message);
        }
      },
        (error: AppError) => {
          Global.setLoadingFlag(false);
          if (error instanceof NotFoundError) {
            this.showMessage(MessageType.Error, "Error", "Requested data not found.");
          }
          else if (error instanceof BadRequestError) {
            this.showMessage(MessageType.Error, "Error", "Unable to process the request.");
          }
          else throw error;
        });
  }

  setSelectedCategory(category: IAssemblyCategory) {
    this.categoryName = category.categoryName;
    this.selectedCategory = category;
  }

  setPage(page: number) {
    if (this.categoryFilteredItems.length > 0 && this.categoryPager.totalPages == 0) { this.categoryPager.totalPages = 1; }
    if (page < 1 || page > this.categoryPager.totalPages) {
      return;
    }
    // get pager object from service
    this.categoryPager = this.pagerService.getPager(this.categoryFilteredItems.length, page, this.pagesize);
    // get current page of items
    this.categoryPagedItems = this.categoryFilteredItems.slice(this.categoryPager.startIndex, this.categoryPager.endIndex + 1);
  }



  filterCategoryRecords(value) {
    this.categoryListFilter = value;
    var valueToSearch = this.categoryListFilter.toUpperCase().trim();
    this.categoryFilteredItems = [];
    if (this.categoryListFilter != "") {
      this.categories.forEach(category => {
        if (category.categoryName.toUpperCase().indexOf(valueToSearch) >= 0) {
          this.categoryFilteredItems.push(category);
        }
      });
    } else {
      this.categoryFilteredItems = this.categories;
    }
    this.setPage(1);
  }



  clearCategoryPanel() {
    this.categoryName = "";
    this.selectedCategory = { id: 0, categoryName: "" };
    //this.clearModelPanel();
    this.newCategory = {
      id: -1,
      categoryName: ""
    }
    //this.getModels(this.newCategory);
  }

  addOrUpdateCategory() {
    if (this.categoryName.trim() === "") return;
    var categoryVal = this.categoryName.trim();
    if (this.selectedCategory.id === 0) {
      //Check if category already exist
      if (this.categories
        .find(c => c.categoryName.toUpperCase().trim() == categoryVal.toUpperCase().trim())) {
        //ToDo: show message category already exist 
        return;
      }
      //new category
      this.newCategory = {
        id: -1,
        categoryName: categoryVal
      }
      Global.setLoadingFlag(true);
      this.assemblyCategoryService.createAssemblyCategory(this.newCategory)
        .subscribe(
          responseData => {
            if (responseData.Success) {

              /* this.newCategory.id = responseData.data[0];
              this.categories.push(this.newCategory);
              this.categoryFilteredItems = this.categories;
              this.clearCategoryPanel();
              this.setPage(1); */
              this.getAssemblyCategories();
              Global.setLoadingFlag(false);
              this.showToastMessage('success','','Category saved successfully.');
            } else {
              Global.setLoadingFlag(false);
              this.showMessage(MessageType.Error, 'Error', 'The specified category already exist.');
              return;
            }
          }
        )
      //ToDo:  remove following code and call getAssemblyCategories
      //this.categories.push(this.newCategory);
    }
    else {
      //update category
      let updateCategory: IAssemblyCategory;
      updateCategory = {
        id: this.selectedCategory.id,
        categoryName: categoryVal
      }

      Global.setLoadingFlag(true);
      this.assemblyCategoryService.updateAssemblyCategory(updateCategory)
        .subscribe(
          responseData => {

            if (responseData.Success) {
              //console.log(responseData);  
              //this.selectedCategory.categoryName = categoryVal;
              this.clearCategoryPanel();
              this.getAssemblyCategories();
              //this.setPage(1);
              Global.setLoadingFlag(false);
              this.showToastMessage('success','','Category updated successfully.');
            } else {
              Global.setLoadingFlag(false);
              this.showMessage(MessageType.Error, 'Error', responseData.Message);
              return;
            }
          });
    }
    this.clearCategoryPanel();
    this.setPage(1);
  }

  deleteCategory(category: IAssemblyCategory) {
    var index = this.categories.findIndex(c => c.id === category.id);
    if (index >= 0) {
    let disposable = this.dialogService.addDialog(MessageBoxComponent, {
      title: "Delete?",
      messageType: MessageType.Question,
      message: "Do you want to delete selected category?"

    }).subscribe((isConfirmed) => {
      if (isConfirmed) {
          this.assemblyCategoryService.deleteAssemblyCategory(category.id)
            .subscribe(
              responseData => {
                if (responseData.Success) {
                  /* this.categories.splice(index, 1);
                  this.categoryFilteredItems = this.categories;
                  this.setPage(1); */
                  this.getAssemblyCategories();
                  Global.setLoadingFlag(false);
                  this.showToastMessage('success','','Category deleted successfully.');
                } else {
                  Global.setLoadingFlag(false);
                  this.showMessage(MessageType.Error, 'Error', responseData.Message);
                  return;
                }
              })
          this.clearCategoryPanel();
          this.setPage(1);
        
      }
    })
  }
}

  showMessage(messageType: MessageType, title: string, message: string) {

    let disposable = this.dialogService.addDialog(MessageBoxComponent, {
      title: title,
      messageType: messageType,
      message: message

    }).subscribe((isConfirmed) => { });
  }

  showToastMessage(severity:string, summary: string, detail:string){
    let message: Message = {
      severity: severity,
      summary: summary,
      detail:detail
    }
    this.messageService.add(message);
  }

}
