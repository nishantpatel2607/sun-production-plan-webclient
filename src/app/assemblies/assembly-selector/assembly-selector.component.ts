import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { AssemblyService } from '../../core/services/assembly.service';
import { PagerService } from '../../core/services/pager.service';
import { IAssembly } from '../../model/assembly';
import { MessageType, MessageBoxComponent } from '../../shared/message-box/message-box.component';
import { DialogService } from 'ng2-bootstrap-modal';
import { AppError } from '../../errorhandlers/app-error';
import { NotFoundError } from '../../errorhandlers/not-found-error';
import { BadRequestError } from '../../errorhandlers/bad-request-error';
import { Global } from '../../core/services/global';
import { IAssemblyCategory } from '../../model/assemblyCategory';
import { AssemblyCategoryService } from '../../core/services/assemblyCategory.service';

@Component({
  selector: 'assembly-selector',
  templateUrl: './assembly-selector.component.html',
  styleUrls: ['./assembly-selector.component.css']
})
export class AssemblySelectorComponent implements OnInit {
  assemblies: IAssembly[];
  assembliesByCategory: IAssembly[];
  assemblyCategories: IAssemblyCategory[];
  errorMessage: string;
  listFilter: string = "";
  //loading: boolean = false;
  pager: any = {};
  pagedItems: IAssembly[];
  filteredItems: IAssembly[];
  @Input('DisableAssembly') disabledAssembly: string;
  selectedAssemblyCategory: IAssemblyCategory;

  //sorting
  key: string = 'assemblyName'; //set default
  reverse: boolean = false;
  sort(key) {
    this.key = key;
    this.reverse = !this.reverse;
  }

  @Output('ItemSelected') ItemSelected = new EventEmitter();

  constructor(private assemblyService: AssemblyService,
    private pagerService: PagerService,
    private dialogService: DialogService,
    private assemblyCategoryService: AssemblyCategoryService) { }

  ngOnInit() {
    this.loadAssemblies();
    this.getAllAssemblyCategories();
  }

  categoryChanged() {
    //alert(this.selectedAssemblyCategory.categoryName);
    this.filterRecords('');
  }

  loadAssemblies() {
    this.assemblyService.getAssemblies()
      .subscribe(assemblyData => {
        if (assemblyData.Success) {
          this.assemblies = assemblyData.data;
          this.assembliesByCategory = assemblyData.data;
          this.filteredItems = this.assemblies;

          this.setPage(1);
          Global.setLoadingFlag(false);
        } else {
          Global.setLoadingFlag(false);
          this.showMessage(MessageType.Error, "Error", assemblyData.Message);
        }
      },
        (error: AppError) => {
          Global.setLoadingFlag(false);
          if (error instanceof NotFoundError) {
            this.showMessage(MessageType.Error, "Error", "Requested data not found.");
          }
          else if (error instanceof BadRequestError) {
            this.showMessage(MessageType.Error, "Error", "Unable to process the request.");
          }
          else throw error;
        });
  }

  setPage(page: number) {
    if (this.filteredItems.length > 0 && this.pager.totalPages == 0) { this.pager.totalPages = 1; }
    if (page < 1 || page > this.pager.totalPages) {
      return;
    }

    // get pager object from service
    this.pager = this.pagerService.getPager(this.filteredItems.length, page);

    // get current page of items
    this.pagedItems = this.filteredItems.slice(this.pager.startIndex, this.pager.endIndex + 1);
  }


  filterRecords(value) {

    this.listFilter = value;
    var valueToSearch = this.listFilter.toUpperCase().trim();
    this.filteredItems = [];
    if (this.listFilter != "") {
      if (this.selectedAssemblyCategory.id === 0) {
        this.filteredItems = this.assemblies.filter((asm) => asm.assemblyName.toUpperCase().indexOf(valueToSearch) >= 0 )
      } else {
        this.filteredItems = this.assemblies.filter((asm) => asm.categoryId === this.selectedAssemblyCategory.id && asm.assemblyName.toUpperCase().indexOf(valueToSearch) >= 0 );
      }
      /* this.assemblies.forEach(assembly => {
        if (assembly.assemblyName.toUpperCase().indexOf(valueToSearch) >= 0) {
          this.filteredItems.push(assembly);
        }
      }); */
    } else {
      if (this.selectedAssemblyCategory.id === 0) {
        this.filteredItems = this.assemblies;
      } else {
        this.filteredItems = this.assemblies.filter((asm) => asm.categoryId === this.selectedAssemblyCategory.id );
      }
    }
    //console.log(this.filteredItems);
    this.setPage(1);
  }

  selectAssembly(assembly: IAssembly) {
    if (assembly.assemblyName !== this.disabledAssembly)
      this.ItemSelected.emit(assembly);
  }

  showMessage(messageType: MessageType, title: string, message: string) {

    let disposable = this.dialogService.addDialog(MessageBoxComponent, {
      title: title,
      messageType: messageType,
      message: message

    }).subscribe((isConfirmed) => { });
  }

  //get all assembly categories
  getAllAssemblyCategories() {
    Global.setLoadingFlag(true);
    this.assemblyCategoryService.getAssemblyCategories().subscribe(
      categoriesData => {
        if (categoriesData.Success) {
          this.assemblyCategories = categoriesData.data;
          Global.setLoadingFlag(false);
        } else {
          Global.setLoadingFlag(false);
          this.showMessage(MessageType.Error, "Error", categoriesData.Message);
        }
      },
      (error: AppError) => {
        Global.setLoadingFlag(false);
        if (error instanceof NotFoundError) {
          this.showMessage(MessageType.Error, "Error", "Requested data not found.");
        }
        else if (error instanceof BadRequestError) {
          this.showMessage(MessageType.Error, "Error", "Unable to process the request.");
        }
        else throw error;
      }, () => {
        let asmCat: IAssemblyCategory = {
          categoryName: 'All Categories',
          id: 0
        }
        this.assemblyCategories.splice(0, 0, asmCat);
        this.selectedAssemblyCategory = this.assemblyCategories[0];
      });
  }
}
