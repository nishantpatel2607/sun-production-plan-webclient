
import { Injectable } from '@angular/core'; 
import { Response,Http, RequestOptions, RequestMethod, Headers } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';
import { IAssemblyCategory } from '../../model/assemblyCategory';
import { HttpClient } from '@angular/common/http';
import { NotFoundError } from '../../errorhandlers/not-found-error';
import { BadRequestError } from '../../errorhandlers/bad-request-error';
import { AppError } from '../../errorhandlers/app-error';
import { IResponse } from './IResponse';
import { Global } from './global';
import { environment } from '../../../environments/environment';

@Injectable()
export class AssemblyCategoryService{
    //private _assemblyCategoryUrl = "./assets/assemblyCategories.json"; 
    
    constructor(private _http: Http){}

    //get all categories
    getAssemblyCategories(): Observable<IResponse>{
        return this._http.get(environment.apiUrl + "AssemblyCategories")
        .map((response: Response) => <IResponse>response.json())
        .catch(this.handleError);
    }

    //get assembly category based on Id
    getAssemblyCategory(id: number) :Observable<IResponse> {
        return this._http.get(environment.apiUrl + "AssemblyCategories/" + id)
        .map((response: Response) => <IResponse>response.json())
        .catch(this.handleError);
    }

    createAssemblyCategory(newAssemblyCategory:IAssemblyCategory) : Observable<IResponse> {
        const options = this.GetOptions();
        let body = JSON.stringify(newAssemblyCategory);
        return this._http.post(environment.apiUrl + "AssemblyCategories",body,options)
            .map((response: Response) => <IResponse>response.json())
            //.do(data => console.log(data.data))
            .catch(this.handleError);
    }

    updateAssemblyCategory(assemblyCategory:IAssemblyCategory) : Observable<IResponse>{
        const options = this.GetOptions();
        let body = JSON.stringify(assemblyCategory);
        return this._http.put(environment.apiUrl + "AssemblyCategories/" + assemblyCategory.id,body,options)
            .map((response: Response) => <IResponse>response.json())
            .do(data => console.log(data))
            .catch(this.handleError);
    }

    deleteAssemblyCategory(id:number) : Observable<IResponse>{
        const options = this.GetOptions();
        return this._http.delete(environment.apiUrl + "AssemblyCategories/" + id,options)
            .map((response: Response) => <IResponse>response.json())
            .do(data => console.log(data))
            .catch(this.handleError);
    }

    private handleError(error: Response) {
        if (error.status === 404) {
            return Observable.throw(new NotFoundError());
        }
        if (error.status === 400) {
            return Observable.throw(new BadRequestError(error.json()));
        }

        return Observable.throw(new AppError(error));
    }

    private GetOptions(): RequestOptions {
        const headers = new Headers();
        headers.append('Content-Type', 'application/json');
        return new RequestOptions({ headers: headers });
    }
}