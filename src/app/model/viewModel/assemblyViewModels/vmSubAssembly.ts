export interface IVMSubAssembly { 
    assemblyId: number;
    subAssemblyId: number;
    subAssemblyName: string;
    qty: number;
    duration: number;
    sequenceNo: number;
}