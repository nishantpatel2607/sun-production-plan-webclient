export interface ISubAssembly { 
    assemblyId: number;
    subAssemblyId: number;
    qty: number;
    sequenceNo: number;
}