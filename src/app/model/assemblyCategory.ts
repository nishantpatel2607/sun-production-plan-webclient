export interface IAssemblyCategory{
    id: number;
    categoryName: string;
}